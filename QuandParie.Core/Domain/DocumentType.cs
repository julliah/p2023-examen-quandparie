﻿namespace QuandParie.Core.Domain
{
    public enum DocumentType
    {
        IdentityProof,
        AddressProof
    }
}