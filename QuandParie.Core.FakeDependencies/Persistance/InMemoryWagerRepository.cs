﻿using QuandParie.Core.Domain;
using QuandParie.Core.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuandParie.Core.FakeDependencies.Persistance
{
    public class InMemoryWagerRepository : IWagerRepository
    {
        public Dictionary<Guid, Wager> Wagers = new();

        public void Save(Wager wager)
            => Wagers[wager.Id] = wager;

        public Task SaveAsync(Wager wager)
        {
            Save(wager);
            return Task.CompletedTask;
        }

        public Task<IReadOnlyList<Wager>> GetWagerHavingOdds(Guid id)
        {
            IReadOnlyList<Wager> wagers = Wagers.Values
                .Where(wager => wager.Odds.Any(odds => odds.Id == id))
                .ToList();
            return Task.FromResult(wagers);
        }

        public Task<IReadOnlyList<Wager>> GetWagerOfUser(string email)
        {
            IReadOnlyList<Wager> wagers = Wagers.Values
                .Where(wager => wager.Customer.Email == email)
                .ToList();
            return Task.FromResult(wagers);
        }
    }
}
